# RLDevRLTut2019
[![RoguelikeDev Does the Roguelike Tutorial 2019 logo](https://i.imgur.com/0FVrglx.png)](https://old.reddit.com/r/roguelikedev/comments/bz6s0j/roguelikedev_does_the_complete_roguelike_tutorial/)  

# Description  
A roguelike game written in python, using kivy and libtcod.  
Developed while [livestreaming](https://twitch.tv/erogue).    
The videos will be archived at both [Twitch](https://www.twitch.tv/collections/2P3vkaHkqhWtvw) and [YouTube](https://www.youtube.com/watch?v=1zpnltmHZ0k&list=PLSX2U_ZE4HukmWK-XrZnkdIA2Zb0j924S).  

# Table of Contents  
1. [Installation](#installation)
1. [Usage](#usage)
1. [Credits](#credits)
1. [License](#license)
1. [Images](#images)

# Installation
* Ensure the following **dependancies** are installed:
	* [python 3.7.3](https://www.python.org/downloads/release/python-373/)
	* [kivy 1.11.1](https://kivy.org/#download)
	* [libtcod 11.0.2](https://bitbucket.org/libtcod/libtcod/downloads/)
* Download the project and extract if zipped and/or tarballed.
# Usage
Running `main.py` will launch the game.
* Windows  
  `py main.py`  

* Unix  
  `python3 main.py`

# Credits
Produced by e.Rogue with support from [the RoguelikeDev community](https://www.reddit.com/r/roguelikedev/).

# License  
Give credit where credit is due.

# Images
![RoguelikeDev Does the Roguelike Tutorial ex. 1 screenshot](https://i.imgur.com/XzJgDgC.png)
