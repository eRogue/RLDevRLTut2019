#! python3.7

import logging
import sys
logging.basicConfig(	#stream = sys.stderr,
						# format = '%(message)s',
						style='{',
						# level = logging.NOTSET,
						level = logging.DEBUG,
						# level = logging.INFO,
						# level = logging.WARNING,
						# level = logging.ERROR,
						# level = logging.CRITICAL,
)


from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.properties import NumericProperty, ObjectProperty, ReferenceListProperty
from kivy.vector import Vector
from kivy.clock import Clock
from kivy.core.window import Window

# import tcod

from input_handler import handle_keycode



class PongPaddle(Widget):
	score = NumericProperty(0)
	
	def bounce_hero(self, hero):
		if self.collide_widget(hero):
			logging.info('Bounce!')
			vx, vy = hero.velocity
			offset = ( hero.center_y - self.center_y) / (self.height / 2)
			bounced = Vector(-1 * vx, vy)
			vel = min(bounced * 1.1, Vector(24, vy))
			hero.velocity = vel.x, vel.y + offset


class Player(Widget):

	velocity_x = NumericProperty(0)
	velocity_y = NumericProperty(0)
	velocity = ReferenceListProperty(velocity_x, velocity_y)

	def move(self):
		self.pos = Vector(*self.velocity) + self.pos
		# self.pos += (-10,0)


	def test(self):
		logging.info('Player.test()!')



class RLGame(Widget):
	
	
	hero = ObjectProperty(None)
	pad1 = ObjectProperty(None)
	pad2 = ObjectProperty(None)
	
	def __init__(self, **kwargs):
		super(RLGame, self).__init__(**kwargs)
		self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
		self._keyboard.bind(on_key_down = self._on_keyboard_down)


	def _keyboard_closed(self):
		self._keyboard.unbind(on_key_down = self._on_keyboard_down)
		self._keyboard = None
		
		
	def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
		print(keycode, modifiers)
		
		commands = handle_keycode(keycode, modifiers)
		for command, value in commands.items():
			print(command, value)
			if command == 'quit':
				quit()
			elif command == 'move':
				self.hero.center_x += value[0]
				self.hero.center_y += value[1]
			elif command == 'toggle_fullscreen':
				logging.info('No fullscreen for you!')
			else: 
				raise LookupError('Unprocessed command received.')
		return True

		
		
	def serve_hero(self, vel=(4, 0)):
		self.hero.center = self.center
		self.hero.velocity = vel

	def update(self, dt):
		
		self.hero.move()
		
		#bounce on paddles
		self.pad1.bounce_hero(self.hero)
		self.pad2.bounce_hero(self.hero)
		
		#bounce on wall
		if (self.hero.y < 0) or (self.hero.top > self.height):
			self.hero.velocity_y *= -1
		
		#score points
		if self.hero.x < self.x:
			self.pad2.score += 1
			self.serve_hero(vel=(4, 0))
			self.status_message()
			
		if self.hero.x > self.width:
			self.pad1.score += 1
			self.serve_hero(vel=(4,0))
			self.status_message()
			
	def on_touch_move(self, touch):
		if touch.x < self.width / 3:
			self.pad1.center_y = touch.y
		if touch.x > self.width - self.width / 3:
			self.pad2.center_y = touch.y
			
	def status_message(self):
		logging.info(f'{self.pad1.score}:{self.pad2.score}')

class RLApp(App):
	def build(self):

		game = RLGame()
		game.serve_hero( vel=(0,0))
		Clock.schedule_interval(game.update, 1/60)
		return game

	def test(self):
		print(f'RLApp.test()! {type(self)}')

if __name__ == '__main__':
	
	RLApp().run()
