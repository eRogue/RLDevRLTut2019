#! python3.7

def handle_keycode(keycode, modifiers):

	if keycode[1] == 'numpad8':
		return {'move': (0, 10)}
	elif keycode[1] == 'numpad2':
		return {'move': (0, -10)}
	elif keycode[1] == 'numpad4':
		return {'move': (-10, 0)}
	elif keycode[1] == 'numpad6':
		return {'move': (10, 0)}

	elif keycode[1] == 'escape':
		return {'quit': True}

	elif keycode[1] == 'enter':
		if 'alt' in modifiers:
			return {'toggle_fullscreen' : True}


	# elif keycode[1] == 'x':
		# return {'error': True}

	return {}


